# mqtt tempsensor

reads temperature data from a bme280 temperature sensor and streams this data to a mqtt broker

## running it
```
usage: mqtt-tempsensor [-h] --id ID [--prom-port PROM_PORT]
                       [--interval INTERVAL] [--mqtt-host MQTT_HOST]
                       [--mqtt-topic MQTT_TOPIC] [--verbose]

If an arg is specified in more than one place, then commandline values
override environment variables which override defaults.

optional arguments:
  -h, --help            show this help message and exit
  --id ID               A unique id to identify this sensor [env var: BME_ID]
  --prom-port PROM_PORT
                        Port to use for prometheus to scrape metrics. Supply 0
                        to disable [env var: BME_PROMPORT]
  --interval INTERVAL   Interval of scrapes in seconds [env var: BME_INTERVAL]
  --mqtt-host MQTT_HOST
                        The MQTT host to send sensor readings to [env var:
                        BME_MQTT_HOST]
  --mqtt-topic MQTT_TOPIC
                        The MQTT topic to send sensor data to [env var:
                        BME_MQTT_TOPIC]
  --verbose             Prints debug logs
```
