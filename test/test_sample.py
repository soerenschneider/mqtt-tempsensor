import unittest
import time
from mqtt_tempsensor import datasample


class TestSample(unittest.TestCase):
    def test_timestamp(self):
        s1 = datasample.Sample(1.0, 2.0, 3.0)
        time.sleep(0.1)
        s2 = datasample.Sample(2.0, 3.0, 4.0)

        self.assertNotEqual(s1.timestamp, s2.timestamp)
