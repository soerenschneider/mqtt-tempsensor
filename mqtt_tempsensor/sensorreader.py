import time
import logging
import json

from mqtt_tempsensor.metrics import (
    PROM_GAUGE_SENSOR_TEMP,
    PROM_GAUGE_SENSOR_PRESSURE,
    PROM_GAUGE_SENSOR_HUMIDITY,
    PROM_GAUGE_SENSOR_TIMESTAMP,
)


class SensorReader:
    """ Accepts a sensor that is continously read from and writes the data back to callbacks. """

    def __init__(self, args, sensor, callbacks=None):
        if not args or not args.id:
            raise ValueError("No ID given")
        self._location = args.id

        self.args = args
        self.sensor = sensor
        self._proceed = True

        if not callbacks:
            callbacks = list()
        self._callbacks = callbacks

    def stop(self):
        self._proceed = False

    def run(self):
        """ Continously fetch data from the sensor and write it to the configured backend(s). """
        #pylint: disable=broad-except
        while self._proceed:
            try:
                sample = self.sensor.read_sample()
                logging.debug("Read data from sensor: %s", sample)

                self.write_metrics(sample)
                json_data = json.dumps(sample.__dict__, ensure_ascii=False)
                for callback in self._callbacks:
                    callback.trigger(json_data)

                time.sleep(self.args.interval)
            except KeyboardInterrupt:
                logging.info("Caught signal...")
                self._proceed = False
            except Exception as error:
                time.sleep(10)
                logging.error("Error: %s", str(error))
        logging.info("Quitting, bye!")

    def write_metrics(self, sensor_data):
        """ Writes the sensor data to the metrics. """

        if not sensor_data:
            return

        PROM_GAUGE_SENSOR_TEMP.labels(location=self._location).set(
            sensor_data.temperature
        )
        PROM_GAUGE_SENSOR_PRESSURE.labels(location=self._location).set(
            sensor_data.pressure
        )
        PROM_GAUGE_SENSOR_HUMIDITY.labels(location=self._location).set(
            sensor_data.humidity
        )
        PROM_GAUGE_SENSOR_TIMESTAMP.labels(
            location=self._location
        ).set_to_current_time()
