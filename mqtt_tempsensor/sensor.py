import smbus2
import bme280

from mqtt_tempsensor.datasample import Sample


class Bme280Sensor:
    """ Reads data from a BME280 sensor. """

    def __init__(self, port=1, address=0x76):
        self._bus = smbus2.SMBus(port)
        self._address = address
        self._calibration = None

        self._calibration = bme280.load_calibration_params(self._bus, self._address)

    def _convert_sensor_data(self, data_sample):
        """ Converts the sensor data to a dict. """
        sensor_data = Sample(
            temperature=data_sample.temperature,
            humidity=data_sample.humidity,
            pressure=data_sample.pressure,
        )

        return sensor_data

    def read_sample(self) -> Sample:
        """ Read the data. """
        sample = bme280.sample(self._bus, self._address, self._calibration)
        return self._convert_sensor_data(sample)
