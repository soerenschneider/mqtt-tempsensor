import datetime
from dataclasses import dataclass


@dataclass
class Sample:
    """ Defines a sample that has been read from the sensor. """

    temperature: float
    humidity: float
    pressure: float
    timestamp: str = ""

    def __post_init__(self):
        self.timestamp = datetime.datetime.now().isoformat()
