import logging

from prometheus_client import Gauge, Counter, start_http_server

_PREFIX = "iot_sensors_bme280"

PROM_GAUGE_SENSOR_TEMP = Gauge(
    "{}_temp_celsius".format(_PREFIX), "Temperature in degrees celsius", ["location"]
)

PROM_GAUGE_SENSOR_PRESSURE = Gauge(
    "{}_pressure_hpa".format(_PREFIX), "Pressure in hpa", ["location"]
)

PROM_GAUGE_SENSOR_HUMIDITY = Gauge(
    "{}_humidity_percent".format(_PREFIX), "Humidity in percent", ["location"]
)

PROM_GAUGE_SENSOR_TIMESTAMP = Gauge(
    "{}_timestamp".format(_PREFIX), "Timestamp of last measurement", ["location"]
)

PROM_COUNTER_MQTT_MSG_ERRORS = Counter(
    "{}_backend_mqtt_msg_send_errors_total".format(_PREFIX),
    "Errors while publishing messages",
    ["location"],
)
PROM_COUNTER_MQTT_RECONNECTS = Counter(
    "{}_backend_mqtt_reconnects_total".format(_PREFIX),
    "Client reconnects",
    ["location"],
)


def setup_prometheus(port: int):
    """ Starts the prometheus http server. """
    logging.info("Starting prometheus http server on port %d", port)
    start_http_server(port)
    logging.info("Successfully set up prometheus server!")
