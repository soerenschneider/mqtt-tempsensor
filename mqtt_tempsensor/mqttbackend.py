import logging
import paho.mqtt.client as mqtt

from mqtt_tempsensor.metrics import (
    PROM_COUNTER_MQTT_MSG_ERRORS,
    PROM_COUNTER_MQTT_RECONNECTS,
)


class MqttBackend:
    def __init__(self, host, location, topic, port=None):
        logging.info("Initializing MQTT backend...")
        if not host or not location or not topic:
            raise ValueError("host, location and topic must be set.")

        self._host = host
        self._location = location
        self._topic = topic.format(location)

        if not port:
            port = 1883
        self._port = port

        self.connect()

    def on_connect(self, client, userdata, flags, result_code):
        # pylint: disable=unused-argument
        logging.info("Connected with result code %s", str(result_code))
        PROM_COUNTER_MQTT_RECONNECTS.labels(self._location).inc()

    def trigger(self, data):
        self.publish(data)

    def publish(self, data):
        logging.debug("Publishing '%s' to %s", data, self._topic)
        info = self._client.publish(self._topic, data)
        if not info.is_published():
            logging.error(
                "Error while publishing message to topic '%s'",
                self._topic,
            )
            PROM_COUNTER_MQTT_MSG_ERRORS.labels(self._location).inc()

    def connect(self):
        self._client = mqtt.Client()
        self._client.on_connect = self.on_connect

        self._client.connect_async(self._host, self._port, 60)
        self._client.loop_start()
        logging.info("Async connecting to %s", self._host)
