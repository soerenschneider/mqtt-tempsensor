import logging
import configargparse

from mqtt_tempsensor.sensor import Bme280Sensor
from mqtt_tempsensor.mqttbackend import MqttBackend
from mqtt_tempsensor.sensorreader import SensorReader
from mqtt_tempsensor.metrics import setup_prometheus


def parse_args():
    """ Parse commandline arguments. """
    parser = configargparse.ArgumentParser(prog="mqtt-tempsensor")

    parser.add_argument(
        "--id",
        action="store",
        env_var="BME_ID",
        help="A unique id to identify this sensor",
        required=True,
    )
    parser.add_argument(
        "--prom-port",
        action="store",
        env_var="BME_PROMPORT",
        help="Port to use for prometheus to scrape metrics. Supply 0 to disable",
        type=int,
        default=9192,
    )
    parser.add_argument(
        "--interval",
        action="store",
        env_var="BME_INTERVAL",
        help="Interval of scrapes in seconds",
        type=int,
        default=30,
    )

    parser.add_argument(
        "--mqtt-host",
        action="store",
        help="The MQTT host to send sensor readings to",
        env_var="BME_MQTT_HOST",
    )
    parser.add_argument(
        "--mqtt-topic",
        action="store",
        env_var="BME_MQTT_TOPIC",
        default="/sensors/bme280/{}",
        help="The MQTT topic to send sensor data to",
    )

    parser.add_argument("--verbose", help="Prints debug logs", action="store_true")

    return parser.parse_args()


def setup_logging(args):
    """ Sets up the logging. """
    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    logging.basicConfig(
        level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
    )


def print_config(args):
    """ Print the supplied configuration """
    logging.info("Started bme temperature sensor")
    logging.info("Using id=%s", args.id)
    logging.info("Using interval=%s", args.interval)
    logging.info("Using prom-port=%s", args.prom_port)


def initialize_mqtt(args):
    """ Build the backend """
    if args.mqtt_host is None:
        return None

    backend = MqttBackend(host=args.mqtt_host, location=args.id, topic=args.mqtt_topic)
    return backend


def main():
    args = parse_args()
    setup_logging(args)
    print_config(args)
    setup_prometheus(args.prom_port)

    callbacks = list()
    mqtt_backend = initialize_mqtt(args)
    if mqtt_backend:
        callbacks.append(mqtt_backend)

    bme = Bme280Sensor()
    sensor_reader = SensorReader(args, bme, callbacks=callbacks)
    sensor_reader.run()


if __name__ == "__main__":
    main()
