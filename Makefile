tests: unittest

venv-test:
	if [ ! -d "venv" ]; then python3 -m venv venv; fi
	venv/bin/pip3 install -r requirements.txt -r requirements-test.txt

.PHONY: venv
venv:
	if [ ! -d "venv" ]; then python3 -m venv venv; fi
	venv/bin/pip3 install -r requirements.txt

unittest:
	venv/bin/python3 -m unittest test/test_*.py

integrationtest:
	venv/bin/python3 -m unittest inttest/itest_*.py

venv-pylint: venv
	venv/bin/pip3 install pylint pylint-exit anybadge

lint:
	venv/bin/pylint --output-format=text mqtt_tempsensor
