import unittest

import argparse
from mqtt_tempsensor.metrics import setup_prometheus, _PREFIX
import requests


class TestIntegration(unittest.TestCase):
    def test_metrics(self):
        setup_prometheus(32323)

        response = requests.get("http://localhost:32323/metrics")
        self.assertTrue(_PREFIX in response.text)
        self.assertTrue(response.ok)
