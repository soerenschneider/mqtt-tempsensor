import unittest

import argparse
import logging
from mqtt_tempsensor.sensorreader import SensorReader
from mqtt_tempsensor.datasample import Sample
from threading import Thread
from time import sleep


class SensorStub:
    """ Stub that writes simple samples that increase each invocation """

    sample = 0

    def read_sample(self):
        self.sample += 1
        return Sample(
            temperature=float(self.sample),
            humidity=float(self.sample),
            pressure=float(self.sample),
        )


class BackendStub:
    """ Stub that stores all samples that it receives. """

    received_messages = list()

    def trigger(self, data):
        self.received_messages.append(data)


class TestIntegration(unittest.TestCase):
    @staticmethod
    def setUpClass():
        loglevel = logging.DEBUG
        logging.basicConfig(
            level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
        )

    def test_integration(self):
        backend = BackendStub()
        callbacks = list()
        callbacks.append(backend)

        sensor = SensorStub()
        args = argparse.Namespace(id="bla", interval=5)
        sensor_reader = SensorReader(args, sensor, callbacks=callbacks)

        self.assertEqual(0, sensor.sample)
        self.assertEqual(0, len(backend.received_messages))

        thread = Thread(target=self.start_it, args=(sensor_reader,))
        thread.start()

        sleep(6)

        sensor_reader.stop()
        thread.join()

        self.assertEqual(2, sensor.sample)
        self.assertEqual(2, len(backend.received_messages))

    def start_it(self, sensor_reader):
        sensor_reader.run()
